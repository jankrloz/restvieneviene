__author__ = 'thrashforner'
from push_notifications.models import APNSDevice, GCMDevice



device = GCMDevice.objects.get(registration_id=gcm_reg_id)
# The first argument will be sent as "message" to the intent extras Bundle
# Retrieve it with intent.getExtras().getString("message")
device.send_message("You've got mail")
# If you want to customize, send an extra dict and a None message.
# the extras dict will be mapped into the intent extras Bundle.
# For dicts where all values are keys this will be sent as url parameters,
# but for more complex nested collections the extras dict will be sent via
# the bulk message api.
device.send_message(None, extra={"foo": "bar"})

def send_user_gsm_message(device, msg):
    device.send_message(None, extra={"foo": "bar"})

def send_user_gsm_message_(device, msg):
    device.send_message(None, extra={"foo": "bar"})


