from django.apps import AppConfig


class GcmConfig(AppConfig):
    name = 'apps.GCM'
