from django.contrib.auth import logout
from push_notifications.models import GCMDevice
from rest_auth.registration.views import RegisterView
from rest_auth.views import LoginView
from rest_framework.response import Response
from apps.usuarios.models import Usuario
from apps.usuarios.serializers import UserSerializer
from rest_framework import generics


class UserList(generics.ListCreateAPIView):
    queryset = Usuario.objects.all()
    serializer_class = UserSerializer


class UserDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Usuario.objects.all()
    serializer_class = UserSerializer

#CLASE CUSTOM DE REGISTRO QUE HEREDA DE LA PRINCIPAL DE DJANGO-REST-AUTH
class customRegisterView(RegisterView):

    def create(self, request, *args, **kwargs):
        print("METODO CREATE EN CUSTOMREGISTERVIEW")
        print(request.data)
        response = super(customRegisterView, self).create(request, *args, **kwargs)
        try:
            if (('registration_id' in request.data)):
                if (request.data['registration_id'] != ""):
                    #En este punto la respuesta fue buena, asi que podemos obtener el usuario y crearle el token
                    usuario = Usuario.objects.get(email=request.data["email"])
                    #Creamos el token del usuario
                    pass
                    gcmdevice, creado = GCMDevice.objects.update_or_create(user=usuario)
                    gcmdevice.registration_id = request.data["registration_id"]
                    gcmdevice.save()
        except Exception as e:
            print(e)

        return response

    def perform_create(self, serializer):
        return super(customRegisterView, self).perform_create(serializer)

######################3     LOGIN REST CUSTOM PARA USUARIOS VALET'S
#     url(r'^login/$', LoginView.as_view(), name='rest_login'),
from rest_framework import status
import json
class LoginValetView(LoginView):

    def get_error_login_response(self):
        msg_= json.loads('{"error":"no eres valet"}')
        return Response(msg_, status=status.HTTP_401_UNAUTHORIZED)

    def post(self, request, *args, **kwargs):
        self.serializer = self.get_serializer(data=self.request.data)
        self.serializer.is_valid(raise_exception=True)
        self.login()
        if (self.user.is_valet):
            return self.get_response()
        logout(request)
        return self.get_error_login_response()

class LoginUserView(LoginView):

    def get_error_login_response(self):
        msg_= json.loads('{"error":"Tu cuenta es de valet, por favor logueate con la otra aplicación"}')
        return Response(msg_, status=status.HTTP_401_UNAUTHORIZED)

    def post(self, request, *args, **kwargs):
        self.serializer = self.get_serializer(data=self.request.data)
        self.serializer.is_valid(raise_exception=True)
        self.login()
        if (not self.user.is_valet):
            return self.get_response()
        logout(request)
        return self.get_error_login_response()
