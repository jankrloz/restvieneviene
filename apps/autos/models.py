from django.db import models

# Create your models here.
from apps.usuarios.models import Usuario

class Auto(models.Model):
    placas = models.CharField(max_length=7, unique=True)
    color = models.CharField(max_length=20)
    modelo = models.CharField(max_length=50)
    marca = models.CharField(max_length=50)
    fecha = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return ("Auto : {0}".format(str(self.placas)))

class Propietario(models.Model):
    dueno = models.ForeignKey(Usuario)
    auto = models.ForeignKey(Auto)
    fecha = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return ("Auto : {0}".format(str(self.dueno.username)))
