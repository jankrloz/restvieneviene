from apps.autos.views import AutosList, AutosDetail, PropietariosList, PropietarioDetail

__author__ = 'thrashforner'
from django.conf.urls import url
#from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns = [
    #url(r'^/$', views.SnippetList.as_view()),
    url(r'^autos/$', AutosList.as_view()),
    url(r'^autos/(?P<pk>[0-9]+)/$', AutosDetail.as_view()),

    url(r'^propietario/$', PropietariosList.as_view()),
    url(r'^propietario/(?P<pk>[0-9]+)/$', PropietarioDetail.as_view()),
]

#urlpatterns = format_suffix_patterns(urlpatterns)