from django.conf.urls import url, include
#from django.contrib.auth.models import User
from django.contrib import admin
from rest_framework import routers

# Routers provide an easy way of automatically determining the URL conf.
from apps.usuarios.views import customRegisterView, LoginValetView

router = routers.DefaultRouter()
#router.register(r'usuarios', UserViewSet)
#router.register(r'groups', GroupViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^', include('apps.autos.urls', namespace='autos_app')),
    url(r'^', include('apps.servicios.urls', namespace='servicios_app')),
    url(r'^', include('apps.usuarios.urls', namespace='usuarios_app')),
    url(r'^', include('apps.GCM.urls', namespace='GCM_app')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    #url(r'^auth/', include('rest_framework_social_oauth2.urls')),

    # para login y registro con django-rest-auth
    url(r'^rest-auth/loginValet/$', LoginValetView.as_view(), name='rest_login'),
    url(r'^rest-auth/', include('rest_auth.urls')),

    url(r'^rest-auth/registration/$', customRegisterView.as_view(), name='rest_register'),
    url(r'^rest-auth/registration/', include('rest_auth.registration.urls')),

]




