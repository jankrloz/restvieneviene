__author__ = 'thrashforner'
import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '7+sjcqbmmf76x4^*di0lyyvw1v#6cbc46mndqv-a%y9vx#*(0j'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []

DJANGO_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.humanize',
    'django.contrib.staticfiles',
    'django.contrib.sites',
)

THIRD_PARTY_APPS = (
    'rest_framework',
    'rest_framework.authtoken',
    'rest_auth',
    'allauth',
    'allauth.account',
    'allauth.socialaccount',
    'rest_auth.registration',
    'push_notifications',

    # 'oauth2_provider',
    # 'social.apps.django_app.default',
    # 'rest_framework_social_oauth2',
)

SITE_ID = 1

LOCAL_APPS = (
    'apps.usuarios',
    'apps.autos',
    'apps.servicios',
    'apps.GCM',
)

INSTALLED_APPS = DJANGO_APPS + LOCAL_APPS + THIRD_PARTY_APPS

MIDDLEWARE_CLASSES = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'restvieneviene.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                # 'social.apps.django_app.context_processors.backends',
                # 'social.apps.django_app.context_processors.login_redirect',
            ],
        },
    },
]

ADMINS = (('Carlos_Alberto', 'lord.rattlehead@hotmail.com'),
          ('Ricardo_Vera', 'isc4.tec@gmail.com'))

MANAGERS = (('Carlos_Alberto', 'lord.rattlehead@hotmail.com'), ("Ricardo_Vera", "isc4.tec@gmail.com"))

WSGI_APPLICATION = 'restvieneviene.wsgi.application'

## email settings
# EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
'''
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'somosrenka@gmail.com'
EMAIL_HOST_PASSWORD = 'karussa040'
EMAIL_USE_TLS = True
DEFAULT_FROM_EMAIL = 'somosrenka@gmail.com'
'''
## backend de autenticacion
## ADD SOCIAL BACKEND
AUTHENTICATION_BACKENDS = (
    # Facebook OAuth2
    # 'social.backends.facebook.FacebookAppOAuth2',
    # 'social.backends.facebook.FacebookOAuth2',
    # django-rest-framework-social-oauth2
    # 'rest_framework_social_oauth2.backends.DjangoOAuth2',

    # Django
    'django.contrib.auth.backends.ModelBackend',
    # '',
)

# Facebook configuration
SOCIAL_AUTH_FACEBOOK_KEY = ''
SOCIAL_AUTH_FACEBOOK_SECRET = ''
# PERMISSIONS
SOCIAL_AUTH_FACEBOOK_SCOPE = ['email']

## Authentication User BCkend
AUTH_USER_MODEL = 'usuarios.Usuario'

# Password validation
# https://docs.djangoproject.com/en/1.9/ref/settings/#auth-password-validators
'''
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]
'''
LANGUAGE_CODE = 'es-MX'

TIME_ZONE = 'America/Mexico_City'

USE_I18N = True
USE_L10N = True
USE_TZ = True

DEFAULT_CHARSET = 'utf-8'
FILE_CHARSET = 'utf-8'

## media files
MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

# static files
STATIC_ROOT = 'static/'
STATIC_URL = '/static/'

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
)

API_SERVER_KEY = "AIzaSyCuCkcL0_hP5sEBNSz_l-cjcXDfLxc8C2A" #oficiales
PUSH_NOTIFICATIONS_SETTINGS = { "GCM_API_KEY": API_SERVER_KEY, }
#from push_notifications.models import APNSDevice, GCMDevice